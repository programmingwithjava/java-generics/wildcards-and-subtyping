import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        /*
        * Below is a basic application of subtyping in Java.
        * inheritance of regular classes follows this rule of subtyping:
        * class B is a subtype of class A if B extends A.
        * */
        B b = new B();
        A a = b;

        /*
        * This rule does not apply to generic types:
        *
        * */
        List<B> listB = new ArrayList<>();
        List<A> listA = listB;//results in compile-time error


        /*
        * Another example:
        *
        * Although Integer is a subtype of Number, List<Integer> is not a subtype of List<Number>
        * and, in fact, these two types are not related.
        * */
        List<Number> listNumber = new ArrayList<>();
        List<Integer> ilstInteger = listNumber;//results in compile-time error

        /*
        * The common parent of List<Number> and List<Integer> is List<?>.
        * We can use a bounded wildcard to create a parent child relation between these two Lists
        *
        * Pay attention that the opposite won't work:
        * */

        List<? extends Integer> listInt = new ArrayList<>();
        List<? extends Number> listNum = listInt;

        List<? extends Number> listNum2 = new ArrayList<>();
        List<? extends Integer> listInt2 = listNum2;//error
    }
}

class A {}
class B extends A {}

/*
* Below will say "unexpected bound". The reason is we have already defined the bounds for E as a type
* parameter for MyList class. No need to define it again later on the same line as a type parameter for
* ArrayList:
* class MyList<E extends Number> extends ArrayList<E extends Number>
*
* See te correct usage below:
* */
class MyList<E extends Number> extends ArrayList<E> {

}

class C {
    /*
    * wildcard can be used as type parameter even if it is not used in the class level
    * */
    List<?> myList = new ArrayList<>();
}

class D<T>{
    /*
    * Below List definition won't work if we don't use the type parameter on the class level for class D.
    * We need a class level or a method level(see below) declaration for T before using it as a type parameter for List.
    * Try and see buy removing "<T>" from the generic class definition of class D.
    * */
    List<T> myList = new ArrayList<>();
}

class E<T>{
    /*
     * example usage of T and ? together in a generic method.
     * the type parameter used in myMethod below hides the type parameter defined at class level
     * for class E above.
     * */
    public <T extends Integer> void myMethod(T t){
        List<T> listN = new ArrayList<>();
        List<? extends Number> listI = listN;
    }
}

class F<T extends Integer>{
    /*
     * using the type parameter used at the class level also for the method below.
     * */
    public void myMethod(T t){
        List<T> listN = new ArrayList<>();
        List<? extends Number> listI = listN;
    }
}

class G<I extends Integer, N extends Number, M extends Integer>{
    /*
     * using the type parameter used at the class level also for the method below.
     * */
    public <Z extends Integer> void myMethod(I i, N n){
        List<I> listI = new ArrayList<>();
        List<? extends Number> listN = listI;
        /*
        * all the below will be like trying to put apples inside a basket made for oranges.
        * and hence they will cause incompatible type errors.
        * */
        //List<M> listM = listI;//error

        /*
        *
        * */
        //List<N> listN2 = listI;//error

        //List<Z> listZ = listI;//error
    }
}